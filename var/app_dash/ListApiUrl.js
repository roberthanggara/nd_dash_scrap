exports.API_PAJAK = "http://36.91.58.53:8088/kominfo/walikota.php"
exports.API_KEPEGAWAIAN = "https://simpeg.malangkota.go.id/serv/get.php"

exports.API_PDAM_PELANGGAN = "https://ncctrial.malangkota.go.id/malang/api/pdammain/get_pelanggan_rekap"
exports.API_PDAM_FTAB = "https://ncctrial.malangkota.go.id/malang/api/pdammain/index_FountainTap"

exports.API_SAMBAT_MAIN = "https://spm.malangkota.go.id/inject/inject_sambat.php"
    exports.URL_PARAM_LINK_ADUAN = `https://spm.malangkota.go.id/inject_sambat/?kategori=aduan-masuk-date&date=`
    exports.URL_PARAM_WEB = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-skpd-date&date=`
    exports.URL_PARAM_SMS = `${this.API_SAMBAT_MAIN}?kategori=sms-aduan-skpd-date&date=`
    exports.URL_PARAM_FULL = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-full-join-date&date=`
    exports.URL_PARAM_KATEGORI = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-jenis-kate-date&date=`
    exports.URL_PARAM_SKPD = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-skpd-date&date=`
    exports.URL_PARAM_TSMS = `${this.API_SAMBAT_MAIN}?kategori=sms-tanggapan-skpd&date=`
    exports.URL_PARAM_TWEB = `${this.API_SAMBAT_MAIN}?kategori=tiket-tanggapan-skpd&date=`

exports.API_PKK = "https://simpkk.malangkota.go.id/inject_pkk/"
    exports.URL_PARAM_PKK_RT = `${this.API_PKK}?kategori=sum-rt-kelurahan`
    exports.URL_PARAM_PKK_RW = `${this.API_PKK}?kategori=sum-rw-kelurahan`
    exports.URL_PARAM_PKK_DW = `${this.API_PKK}?kategori=sum-dasawisma-kelurahan`
    exports.URL_PARAM_PKK_ANG_DW = `${this.API_PKK}?kategori=sum-ang-dasawisma-kelurahan`
    exports.URL_PARAM_PKK_PY = `${this.API_PKK}?kategori=sum-posyandu-kelurahan`
    exports.URL_PARAM_PKK_KP = `${this.API_PKK}?kategori=sum-kooperasi-kelurahan`

exports.API_SPM = "https://spm.malangkota.go.id/dataapi/ws/"

    exports.URL_PARAM_SPM_P = `${this.API_SPM}kel_data_now`
    exports.URL_PARAM_SPM_PV = `${this.API_SPM}verif_data_now`
    exports.URL_PARAM_SPM_DN = `${this.API_SPM}dinsos_data_now`
    exports.URL_PARAM_SPM_PNY = `${this.API_SPM}penyakit_data_all`
    exports.URL_PARAM_SPM_RS = `${this.API_SPM}rs_data_all`

    // inject spm all
    exports.URL_INJECT_PARAM_SPM_P = `${this.API_SPM}kel_data_all`
    exports.URL_INJECT_PARAM_SPM_PV = `${this.API_SPM}verif_data_all`
    exports.URL_INJECT_PARAM_SPM_DN = `${this.API_SPM}dinsos_data_all`
