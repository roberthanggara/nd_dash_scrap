/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 13:37:24
 * @desc [library for generate date values]
 */

// global lib


// local lib
const { connect2 } = require('../../lib/mysql/bin/connection')
const date = require('../../lib/datetime/date')


// variable
const TAG = "Default Model"

var date_in = date.date_full

exports.check_all = (DB_NAME, tbl_name, where, resolve) => {
    var method = ": check_all: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+tbl_name+" WHERE periode = ? and tipe = ? "
    // var where = [0]

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
        
}

exports.check_cur = (DB_NAME, tbl_name, resolve) => {
    var method = ": check_all: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+tbl_name+" WHERE 1"
    var where = [0]

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
        
}

exports.save_sql_sum = (DB_NAME, tbl_name, data, resolve) => {
    var method = ": save_sql_sum: "
    console.log(TAG+method+"insert")

    var sql_statement = "INSERT INTO "+tbl_name+" VALUES ( ? ) "
    var where = [data]
    
    // return connect(DB_NAME, schemas.insert_cur_sum().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.save_sql_all = (DB_NAME, tbl_name, data, resolve) => {
    var method = ": save_sql_all: "
    console.log(TAG+method+"insert")

    var sql_statement = "INSERT INTO "+tbl_name+" VALUES ? "
    var where = [data]
    
    // return connect(DB_NAME, schemas.insert_cur_all().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            // console.log(callback)
            resolve(callback)
        })
} 

exports.delete_sql_all = (DB_NAME, tbl_name, resolve) => {
    var method = ": delete_sql_all: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+tbl_name+" WHERE 1"
    var where = [0]
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.delete_sql_all_seris = (DB_NAME, tbl_name, where, resolve) => {
    var method = ": delete_sql_all: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+tbl_name+" WHERE periode = ? and tipe = ?"
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

