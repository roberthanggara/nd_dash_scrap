/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:21:43
 * @desc [library for generate date values]
 */

// pkk
const PkkAngDwMg = require("../controller/pkk/PkkAngDwMg")
const PkkAngDwSql = require("../controller/pkk/PkkAngDwSql")

const PkkDwMg = require("../controller/pkk/PkkDwMg")
const PkkDwSql = require("../controller/pkk/PkkDwSql")

const PkkKpMg = require("../controller/pkk/PkkKpMg")
const PkkKpSql = require("../controller/pkk/PkkKpSql")

const PkkPyMg = require("../controller/pkk/PkkPyMg")
const PkkPySql = require("../controller/pkk/PkkPySql")

const PkkRtMg = require("../controller/pkk/PkkRtMg")
const PkkRtMg = require("../controller/pkk/PkkRtMg")

const PkkRwMg = require("../controller/pkk/PkkRwMg")
const PkkRwSql = require("../controller/pkk/PkkRwSql")

const runApps = () =>{
    PkkAngDwMg.set_data()
    PkkAngDwSql.set_data()

    PkkDwMg.set_data()
    PkkDwSql.set_data()

    PkkKpMg.set_data()
    PkkKpSql.set_data()

    PkkPyMg.set_data()
    PkkPySql.set_data()

    PkkRtMg.set_data()
    PkkRtMg.set_data()

    PkkRwMg.set_data()
    PkkRwSql.set_data()
}

runApps()