/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:22:15
 * @desc [library for generate date values]
 */

// pajak
const pajakMg = require("../controller/pajak/PajakMg")
const pajakSql = require("../controller/pajak/PajakSql")


const runApps = () =>{
    pajakMg.set_data()
    pajakSql.set_data()

}

runApps()