/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 11:53:40
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Default")

const TAG = "PAJAK_MONGO"

const mongoPath = MongoPath.PATH_PAJAK

console.time("run-apps")

exports.set_data = async () => {
    var method = ": mongo: set_data_mongo: ";

    var data = await get_data(ListApiUrl.API_PAJAK)
    // console.log(JSON.stringify(data_raw));

    var main_data = []
    
    var sum_target = 0
    var sum_realisasi = 0

    // console.log(data)
    var sts = false
    var msg = "error"


    if(data.sts){
        main_data = data.data

        var row = 1
        for (let i in main_data) {
            var target = parseInt(main_data[i].Target)
            var realisasi = parseInt(main_data[i].Realisasi)
        
            sum_target += target
            sum_realisasi += realisasi

            row++
        }

        sts = true
        msg = "success"

        var data_sum = {"sum_target": sum_target, "sum_realisasi": sum_realisasi}

        var check = await check_data_periode()
        if(check){
            if(check.sts){
                console.log(TAG+method+" method: insert")
                const insert_sum = await save_sum_mongo(data_sum)
                const insert_all = await save_all_mongo(main_data)
            }else{
                console.log(TAG+method+" method: update")
                const update_sum = await update_sum_mongo(data_sum)
                const update_all = await update_all_mongo(main_data)
                // console.log(update_sum)
            }
        }
    }

}

const check_data_periode = () => {
    var method = ": mongo: check_data_periode: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_ALL

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        mongoModels.find({ periode: date.get_date()}, function (err, docs) {
            var return_tmp = {"sts": false, "msg": err, "data":{}}
            
            if (!err) {
                if (docs.length == 0)
                    return_tmp = {"sts": true, "msg": "success", "data":docs}
            }else{
                console.log(TAG+method+"error: "+ err)
            }
            // console.log(docs)
            resolve(return_tmp)
        });
    })
} 

const save_all_mongo = (data) => {
    var method = ": mongo: data_all: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_ALL

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        var msg = new mongoModels(arr_data)

        msg.save().
            then(doc => {
                console.log(TAG+method+"success: "+doc.id)
            }).catch(err => {
                console.log(TAG+method+"error: "+ err)
            })
        resolve(msg)
    })
}

const save_sum_mongo = (data) => {
    var method = ": mongo: data_sum: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_SUM

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        var msg = new mongoModels(arr_data)

        msg.save().
            then(doc => {
                console.log(TAG+method+"success: "+doc.id)
            }).catch(err => {
                console.log(TAG+method+"error: "+ err)
            })
        resolve(msg)
    })
}

const update_all_mongo = (data) => {
    var method = ": mongo: update_all_mongo: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_ALL

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)


        const where = {periode: date.get_date()};
        mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
            // console.log(docs);
            var log_msg = TAG+method+"error: "+err;
            var return_tmp = {"sts": false, "msg": err}
            
            if (!err) {
                if (docs)
                    return_tmp = {"sts": true, "msg": "success"}

                    log_msg = TAG+method+"success: 1";
            }
            
            console.log(log_msg)

            resolve(return_tmp)
        })
    })
}

const update_sum_mongo = (data) => {
    var method = ": mongo: update_sum_mongo: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_SUM

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)


        const where = {periode: date.get_date()};
        mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
            // console.log(docs);
            var log_msg = TAG+method+"error: "+err;
            var return_tmp = {"sts": false, "msg": err}
            
            if (!err) {
                if (docs)
                    return_tmp = {"sts": true, "msg": "success"}

                    log_msg = TAG+method+"success: 1";
            }
            
            console.log(log_msg)

            resolve(return_tmp)
        })
    })
}

// module.exports = {}
// this.set_data()