/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 11:57:19
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data, get_data2} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const PajakModel = require("../../var/model_sql/PajakModel")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')

const TAG = "PAJAK_SQL"
var date_in = date.date_full

console.time("run-apps")

exports.set_data = () => {
    var method = ": set_data_sql: ";

    var data = get_data2(ListApiUrl.API_PAJAK, async (callback)=>{
        // console.log(callback)

        var main_data = []
        
        var sum_target = 0
        var sum_realisasi = 0

        // console.log(data)
        var sts = false
        var msg = "error"

        var arr_cur_all = []
        var arr_cur_sum = []

        var arr_seris_all = []
        var arr_seris_sum = []

        // console.log(callback)

        if(callback.sts){
            main_data = callback.data

            // ----------------------logic data processing-------------------------
            var row = 1
            for (let i in main_data) {
                var target = parseInt(main_data[i].Target)
                var realisasi = parseInt(main_data[i].Realisasi)

                var tmp_sql = [row, i, target, realisasi]
                arr_cur_all.push(tmp_sql)

                arr_seris_all.push([row, date_in, i, target, realisasi])
            
                sum_target += target
                sum_realisasi += realisasi

                row++
            }
            // ----------------------logic data processing-------------------------

            arr_cur_sum = ["0", sum_target.toString(), sum_realisasi.toString()]

            arr_seris_sum = ["0", date_in, sum_target.toString(), sum_realisasi.toString()]

            

            await Promise.all(
                [
                    ins_current_sum(arr_cur_sum),
                    ins_current_all(arr_cur_all),
                    ins_seris_sum(arr_seris_sum),
                    ins_seris_all(arr_seris_all)
                ]
                ).then((values) => {
                // console.log(values);
                console.timeEnd("run-apps")
              });

            // PajakModel.check_single_sum(resolve => {console.log(resolve)})
            
        }

    })


    const ins_current_sum = (arr_cur_sum) => {
        return new Promise(cb => {
            PajakModel.check_single_sum(resolve => {
                if(resolve.data.length == 0){
                    console.log(TAG+method+"insert_sum: "+resolve.status)
                    PajakModel.save_single_sql_sum(arr_cur_sum, resolve_ins =>{
                        // console.log(resolve_ins)
                    })
                }else{
                    console.log(TAG+method+"update")
                    PajakModel.delete_single_sum(resolve_del =>{
                        if(resolve_del.status){
                            // console.log(resolve_del)
                            PajakModel.save_single_sql_sum(arr_cur_sum, resolve_ins =>{})
                        }
                    })
                    
                }
            })
        })
    }

    const ins_current_all = (arr_cur_all) => {
        return new Promise(cb => {
            PajakModel.check_single_all(resolve => {
                // console.log(resolve)
                if(resolve.data.length == 0){
                    console.log(TAG+method+"insert_all: "+resolve.status)
                    PajakModel.save_single_sql_all(arr_cur_all, resolve_ins =>{
                        // console.log(resolve_ins)
                    })
                }else{
                    console.log(TAG+method+"update_all")
                    PajakModel.delete_single_all(resolve_del =>{
                        if(resolve_del.status){
                            // console.log(resolvel)
                            PajakModel.save_single_sql_all(arr_cur_all, resolve_ins =>{})
                        }
                    })
                }
            })
        })
    }

    const ins_seris_sum = (arr_seris_sum) => {
        return new Promise(cb => {
            PajakModel.check_seris_sum(resolve => {
                if(resolve.data.length == 0){
                    console.log(TAG+method+"insert_seris_sum: "+resolve.status)
                    PajakModel.save_seris_sql_sum(arr_seris_sum, resolve_ins =>{
                        // console.log(resolve_ins)
                    })
                }else{
                    console.log(TAG+method+"update_seris_sum")
                    PajakModel.delete_seris_sum(resolve_del =>{
                        if(resolve_del.status){
                            // console.log(resolve_del)
                            PajakModel.save_seris_sql_sum(arr_seris_sum, resolve_ins =>{})
                        }
                    })
                    
                }
            })
        })
    }
    

    const ins_seris_all = (arr_seris_all) => {
        return new Promise(cb => {
            PajakModel.check_seris_all(resolve => {
                if(resolve.data.length == 0){
                    console.log(TAG+method+"insert_seris_all: "+resolve.status)
                    console.log(`${method}`)
                    PajakModel.save_seris_sql_all(arr_seris_all, resolve_ins =>{
                        // console.log(resolve_ins)
                    })
                }else{
                    console.log(TAG+method+"update_seris_all")
                    PajakModel.delete_seris_all(resolve_del =>{
                        if(resolve_del.status){
                            // console.log(resolve_del)
                            PajakModel.save_seris_sql_all(arr_seris_all, resolve_ins =>{})
                        }
                    })
                    
                }
            })
        })
    }

}


// this.set_data()