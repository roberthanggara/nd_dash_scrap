/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 10:41:05
 * @desc [library for generate date values]
 */

// global lib


// local lib
const model = require("../../var/model_sql/SpmModel")

const TAG = "EXTN_SPM"


exports.insert_data = (DB_NAME, tbl_name, data) => {
    return new Promise(cb => {
        model.check_curent(DB_NAME, tbl_name, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_curent(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_curent(DB_NAME, tbl_name, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_curent(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}


exports.insert_data_seris = (DB_NAME, tbl_name, data, where) => {
    return new Promise(cb => {
        model.check_seris(DB_NAME, tbl_name, where, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_seris(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log()
                    // console.log(resolve_ins.message)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_seris(DB_NAME, tbl_name, where, resolve_del =>{
                    // console.log(resolve_del)
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_seris(DB_NAME, tbl_name, data, resolve_ins =>{
                            // console.log(resolve_ins)
                        })
                    }
                })
                
            }
        })
    })
}
