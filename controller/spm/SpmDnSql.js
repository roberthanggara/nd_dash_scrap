/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 10:47:56
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const ExtnTask = require("./ExtnSpmSingle")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const {SPM_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")

const TAG = "SPM_PNY_SQL"

var date_in = date.date_full
var no_sparate_date = date.no_sparate_date

const DB_NAME = MysqlDbParam.SPM_DB_NAME

const TABLE_NAME_CUR = MysqlDbParam.SPM_CUR_DN
const TABLE_NAME_SER = MysqlDbParam.SPM_SERIS_DN


console.time("run-apps")

// console.log(ListApiUrl.URL_PARAM_SPM_DN)
exports.set_data = () => {
    var method = ": set_data: ";
    // console.log(TAG+method+"error: ")
    // id
    // th
    // bln
    // val


    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_SPM_DN, SPM_BASIC_AUTH, async (callback)=>{
        console.log(callback)
        var main_data = []
        
        let id = `${no_sparate_date}`

        var arr_seris = [id, date_in, date.get_single_year, date.get_single_month+1, "0"]
        var arr_curr = [id, date.get_single_year, date.get_single_month+1, "0"]
        
        if(callback.sts){
            main_data = callback.data
            // ----------------------logic data processing-------------------------
            
            if(main_data.length != 0 ){
                // console.log("ok")
                arr_seris = [id, date_in, date.get_single_year, date.get_single_month+1, main_data.data]
                arr_curr = [id, date.get_single_year, date.get_single_month+1, main_data.data]
            }

            let where = [date.get_single_year, date.get_single_month+1]

            // console.log(arr_seris)
            // console.log(arr_curr)
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnTask.insert_data(DB_NAME, TABLE_NAME_CUR, arr_curr),
                        ExtnTask.insert_data_seris(DB_NAME, TABLE_NAME_SER, arr_seris, where)
                    ]
                ).then((values) => {
                    // console.log(values);
                    console.log(TAG+method+"end-run: ")
                    console.timeEnd("run-apps")
                }).catch(err => {
                    console.log(TAG+method+"error: "+ err)
                    console.timeEnd("run-apps")
                });
            
        }

    })




}


this.set_data()