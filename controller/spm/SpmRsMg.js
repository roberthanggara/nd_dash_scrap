/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 15:09:34
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Default")
const {SPM_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")

const TAG = "KEPEGAWAIAN_MONGO"

const mongoPath = MongoPath.PATH_SPM

console.time("run-apps")

exports.set_data = () => {
    var method = ": mongo: set_data: ";

    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_SPM_RS, SPM_BASIC_AUTH, async callback =>{
        var main_data = {
            "th": date.get_single_year,
            "periode": date.get_single_month,
            "val": 0
        }
        
        if(callback.data.lengt > 0){
            main_data = callback
        }
        // console.log(main_data)
        

        const insert_data_seris = check_data_seris(resolve=>{
            // console.log(resolve)
            if(resolve.data == 0){
                console.log(TAG+method+" method: insert")
                save_all_mongo(main_data, rtrn_save =>{})
            }else{
                console.log(TAG+method+" method: update")
                update_all_mongo(main_data, rtrn_update =>{})
            }
        })

        await Promise.all([insert_data_seris])
        .then((values) => {
            console.log(values);
            console.timeEnd("run-apps")
        });
            
    })
}

const check_data_seris = (resolve) => {
    var method = ": mongo: check_data_periode: ";
    var collection = MongoPath.CL_SPM_RS
    var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

    mongoModels.find({ periode: date.get_date()}, function (err, docs) {
        var return_tmp = {"sts": false, "msg": err, "data":{}}
        
        if (!err) {
            if (docs.length == 0)
                return_tmp = {"sts": true, "msg": "success", "data":docs}
        }else{
            console.log(TAG+method+"error: "+ err)
        }
        // console.log(docs)
        resolve(return_tmp)
    });
} 

const save_all_mongo = (data, rtrn) => {
    // return new Promise(rtrn => {
        var method = ": mongo: save_all_mongo: ";
        var collection = MongoPath.CL_SPM_RS

        var arr_data = {
            "sources":ListApiUrl.URL_PARAM_SPM_RS,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        var msg = new mongoModels(arr_data)

        msg.save().
            then(doc => {
                console.log(TAG+method+"success: "+doc.id)
            }).catch(err => {
                console.log(TAG+method+"error: "+ err)
            })
            
        rtrn(msg)
    // })
}

const update_all_mongo = (data, rtrn) => {
    // return new Promise(rtrn => {
        var method = ": mongo: update_all_mongo: "
        var collection = MongoPath.CL_SPM_RS

        var arr_data = {
            "sources":ListApiUrl.URL_PARAM_SPM_RS,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)


        const where = {periode: date.get_date()};
        mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
            // console.log(docs);
            var log_msg = TAG+method+"error: "+err;
            var return_tmp = {"sts": false, "msg": err}
            
            if (!err) {
                if (docs)
                    return_tmp = {"sts": true, "msg": "success"}

                    log_msg = TAG+method+"success: 1";
            }
            
            console.log(log_msg)

            rtrn(return_tmp)
        })
    // })
}


this.set_data()
