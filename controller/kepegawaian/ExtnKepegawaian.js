/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 09:33:07
 * @desc [library for generate date values]
 */

// global lib


// local lib
const model = require("../../var/model_sql/DefaultModel")
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")

const DB_NAME = MysqlDbParam.KAPEG_DB_NAME

const TAG = "KEPEGAWAIAN_CUR_AGAMA"


exports.insert_data = (tbl_name, data) => {
    return new Promise(cb => {
        model.check_cur(DB_NAME, tbl_name, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_all(DB_NAME, tbl_name, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}


exports.insert_data_seris = (tbl_name, data) => {
    return new Promise(cb => {
        model.check_cur(DB_NAME, tbl_name, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_all_seris(DB_NAME, tbl_name, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}
