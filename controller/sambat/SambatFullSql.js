/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 08:39:44
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const ExtnTask = require("./ExtnSambat")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const {SAMBAT_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")

const TAG = "PAJAK_SQL"

var date_in = date.date_full
var no_sparate_date = date.no_sparate_date

const DB_NAME = MysqlDbParam.SAMBAT_DB_NAME

const TABLE_NAME = MysqlDbParam.SAMBAT_FULL


console.time("run-apps")

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_pajak = () => {
    var method = ": set_pajak: ";
    // console.log(TAG+method+"error: ")

    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_FULL, SAMBAT_BASIC_AUTH, async (callback)=>{
        // console.log(callback)
        var main_data = []

        var arr_list = []
        
        if(callback.sts){
            main_data = callback.data

            // ----------------------logic data processing-------------------------
            var row = 1

            var all_pegawai = 0
            for (let i in main_data) {
                let id = `${no_sparate_date}_${i}`

                arr_list.push([id, date_in, `full`, main_data[i].jenis, main_data[i].kategori, main_data[i].nama_skpd, main_data[i].jml_aduan])
                    
                row++
            }

            let where = [date_in, "full"]

            // console.log(arr_list)
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnTask.insert_data_seris(DB_NAME, TABLE_NAME, arr_list, where)
                    ]
                ).then((values) => {
                    // console.log(values);
                    console.log(TAG+method+"end-run: ")
                    console.timeEnd("run-apps")
                }).catch(err => {
                    console.log(TAG+method+"error: "+ err)
                    console.timeEnd("run-apps")
                });
            
        }

    })




}


this.set_pajak()